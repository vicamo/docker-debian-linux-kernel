#!/bin/bash

set -Eeuo pipefail
echo "Running '$@' ..."

SOURCE_DIR=/source
OUTPUT_DIR=/output
BUILD_DIR=/build

ARCH=${ARCH:-$(dpkg --print-architecture)}
# FEATURESET
# FLAVOUR

while [ "$#" -gt 0 ]; do
  case "$1" in
    --host-arch)
      shift; ARCH=$1; ;;
    --host-featureset)
      shift; FEATURESET=$1 ;;
    --host-flavour)
      shift; FLAVOUR=$1 ;;
    *) break;;
  esac
  shift;
done

test -f "${SOURCE_DIR}/debian/rules" \
  || (echo "Missing debian folder under '${SOURCE_DIR}'." >&2; exit 1)

sudo mkdir -p "${BUILD_DIR}"
sudo chown ${KDEVEL_USER} "${BUILD_DIR}"

cd "${SOURCE_DIR}"
UPSTREAM_VERSION=$(dpkg-parsechangelog -SVersion | sed -e 's,-[^-]*$,,')

cd "${BUILD_DIR}"
if [ -f "${SOURCE_DIR}/linux_${UPSTREAM_VERSION}.orig.tar.xz" ]; then
  cp "${SOURCE_DIR}/linux_${UPSTREAM_VERSION}.orig.tar.xz" "${BUILD_DIR}";
elif wget --quiet --spider https://deb.debian.org/debian/pool/main/l/linux/linux_${UPSTREAM_VERSION}.orig.tar.xz; then
  wget --no-verbose https://deb.debian.org/debian/pool/main/l/linux/linux_${UPSTREAM_VERSION}.orig.tar.xz;
else
  wget --no-verbose --output-document - \
      https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/snapshot/linux-${UPSTREAM_VERSION}.tar.gz \
    | gzip -cd | xz --stdout > linux_${UPSTREAM_VERSION}.orig.tar.xz;
fi

cp -a "${SOURCE_DIR}" "${BUILD_DIR}/linux"
cd "${BUILD_DIR}/linux"

IFS=$'\n' && for varenv in $(env); do
  if test -z "${varenv##DEB*}" -o -z "${varenv##DPKG_*}"; then
    echo "export ${varenv%%=*}=\"${varenv#*=}\"";
  fi;
done > "${BUILD_DIR}/env.sh"
echo "export \$(dpkg-architecture --host-arch $ARCH)" >> "${BUILD_DIR}/env.sh"
# Enable build in parallel
echo "export MAKEFLAGS=\"--jobs=$(($(nproc)*2))\"" >> "${BUILD_DIR}/env.sh"
# Disable -dbg (debug) package is only possible when distribution="UNRELEASED" in debian/changelog
(if [ "$(dpkg-parsechangelog --show-field Distribution)" = "UNRELEASED" ]; then
  echo "export DEBIAN_KERNEL_DISABLE_DEBUG=yes";
else
  echo "export DEBIAN_KERNEL_DISABLE_DEBUG=";
fi) >> "${BUILD_DIR}/env.sh"
source "${BUILD_DIR}/env.sh"

# This target is made to fail intentionally
fakeroot make -f debian/rules clean || true
sudo mk-build-deps --install --remove --host-arch $ARCH
if [ -n "$(cat debian/config/defines | grep compiler:)" ]; then
  sudo apt-get install --no-install-recommends --yes \
      $(cat debian/config/defines | grep compiler: | awk '{print $2}');
fi

fakeroot make -f debian/rules orig
fakeroot make -f debian/rules source
fakeroot make -f debian/rules.gen setup${ARCH:+_${ARCH}${FEATURESET:+_${FEATURESET}${FLAVOUR:+_${FLAVOUR}}}}
touch "${BUILD_DIR}/.done-setup"

if [ "$#" -eq 0 ]; then
  echo "Environment setup completed. Sleep forever. Run \`docker exec <container> bash -c 'source ${BUILD_DIR}/env.sh; ...'\` to continue.";
  sleep infinity;
else
  "$@";
fi

rm -rf "${BUILD_DIR}"/{orig,linux,env.sh,.done-setup,linux_*.orig.tar.xz};
if [ -n "$(find "${BUILD_DIR}" -mindepth 1)" ]; then
  tar -C "${BUILD_DIR}" -cf - . | sudo tar -C "${OUTPUT_DIR}" -xf -;
fi
echo "Contents in ${OUTPUT_DIR}:"
find "${OUTPUT_DIR}"
